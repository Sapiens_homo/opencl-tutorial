OpenCL Tutorial
==================

This is a simple tutorial for OpenCL in Chinese(Traditional).

If you have any question, create an `Issue` please.

INDEX
-----------------

### [Chapter 0. Introduction](ch0/README.md)

- [0. Style, symbols and syntax in this book](ch0/0.md)
- [1. History of OpenCL](ch0/1.md)
- [2. Applications](ch0/2.md)
- [3. Architecture](ch0/3.md)
- [4 Difference between C and OpenCL](ch0/4.md)

### [Chapter 1. Syntax](ch1/README.md)

- [0. Reserved Words](ch1/0.md)
- [1. Work Groups and Memory Management](ch1/1.md)
- [2. Data Types](ch2.2.md)
- [3. Statements](ch1/3.md)
- [4. Functions and Kernels](ch1/4.md)

### [Chapter 2. Examples](ch2/README.md)

- [0. Devices and Platforms](ch2/0.md)
- [1. One-dimensional Example](ch2/1.md)
- [2. Two-dimensional Example](ch2/2.md)
