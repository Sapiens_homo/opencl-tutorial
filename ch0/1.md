0-1. History of OpenCL
==========================

幹誰管他的歷史
OpenCL 1.0 由 Apple 於 28 Aug 2009 和 Mac OS X Snow Leopard 一同發佈

| Version   | Date          |
| -------   | ----          |
| 1.0       | 28 08 2009    |
| 1.1       | 14 06 2010    |
| 1.2       | 15 11 2011    |
| 2.0       | 18 11 2013    |
| 2.1       | 16 11 2015    |
| 2.2       | 16 05 2017    |
| 3.0       | 30 09 2020    |
