Chapter 0. Introduction
==============

- [0. Style, symbols and syntax in this book](0.md)
- [1. History of OpenCL](1.md)
- [2. Applications](2.md)
- [3. Architecture](3.md)
- [4. Difference between C and OpenCL](4.md)

[Index](../README.md)
