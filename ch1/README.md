Chapter 1. Syntax
=====================

- [0. Reserved Words](0.md)
- [1. Work Groups and Memory Management](1.md)
- [2. Data Types](2.md)
- [3. Statements](3.md)
- [4. Functions and Kernels](4.md)

[Index](../README.md)
