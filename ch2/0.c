#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

int main( void ){
    cl_platform_id *platforms = NULL;
    cl_device_id *devices = NULL;
    cl_uint num_platforms = 0;
    cl_uint num_devices = 0;

    cl_uint i, j;
    cl_int err = 0;

    char* name = NULL;
    size_t name_len;

    err = clGetPlatformIDs( 0, NULL, &num_platforms );
    if( err < 0 ){ goto ERR; }

    platforms = malloc( sizeof( cl_platform_id ) * num_platforms );

    err = clGetPlatformIDs( num_platforms, platforms, NULL );
    if( err < 0 ){ goto ERR; }

    for( i = 0; i < num_platforms; i++ ){
        err = clGetPlatformInfo( *( platforms + i ), CL_PLATFORM_NAME,
                0, NULL, &name_len );
        if( err < 0 ){ goto ERR; }
        name = malloc( name_len );
        err = clGetPlatformInfo( *( platforms + i ), CL_PLATFORM_NAME,
                name_len, name, NULL );
        if( err < 0 ){ goto ERR; }
        printf( "Platform #%02u: %s\n", i, name );
        free( name );
        name = NULL;
        err = clGetDeviceIDs( *( platforms + i ), CL_DEVICE_TYPE_ALL,
                0, NULL, &num_devices );
        if( err < 0 ){ goto ERR; }
        devices = malloc( sizeof( cl_device_id ) * num_devices );
        err = clGetDeviceIDs( *( platforms + i ), CL_DEVICE_TYPE_ALL,
                num_devices, devices, NULL );
        if( err < 0 ){ goto ERR; }
        for( j = 0; j < num_devices; j++ ){
            err = clGetDeviceInfo( *( devices + j ), CL_DEVICE_NAME,
                0, NULL, &name_len );
            if( err < 0 ){ goto ERR; }
            name = malloc( name_len );
            err = clGetDeviceInfo( *( devices + j ), CL_DEVICE_NAME,
                name_len, name, NULL );
            if( err < 0 ){ goto ERR; }
            printf( "\t#%02u: %s\n", j, name );
            free( name );
            name = NULL;
        }
        free( devices );
        devices = NULL;
    }
ERR:
    free( platforms );
    free( devices );
    free( name );
    return err;
}
