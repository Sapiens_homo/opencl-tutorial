#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CL/cl.h>
#include <math.h>

int main( void ){
	char* const vec_source = "\
			   __kernel void add (\
					   __global const double *a,\
					   __global const double *b,\
					   __global double *c, uint n\
					   ){\
				   uint i = get_global_id(0);\
				   if( i < n ){\
					   *( c + i ) = *( a + i ) + *( b + i );\
					   /* c[i] = a[i] + b[i] */\
				   }\
			   }";

	size_t source_len = strlen( vec_source );
	cl_platform_id *platforms = NULL;
	cl_device_id device;
	cl_context context = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_command_queue queue = NULL;

	cl_uint num_platform;

	cl_int err = 0;

	double *vecA = NULL;
	double *vecB = NULL;
	double *vecC = NULL;

	cl_mem vecA_cl = NULL;
	cl_mem vecB_cl = NULL;
	cl_mem vecC_cl = NULL;

	size_t local_size;
	size_t global_size;

	unsigned int vec_len;

	unsigned int i;

	register char* error_log = NULL;
	size_t error_len;

	err = clGetPlatformIDs( 0, NULL, &num_platform );
	if( err < 0 ){ goto ERR; }
	platforms = malloc( sizeof( cl_platform_id ) * num_platform );
	err = clGetPlatformIDs( num_platform, platforms, NULL );
	if( err < 0 ){ goto ERR; }

	for( i = 0; i < num_platform; i++ ){
		err = clGetDeviceIDs( *( platforms + i ), CL_DEVICE_TYPE_GPU,
				1, &device, NULL );
		switch (err){
			case CL_SUCCESS:
				goto OUTSIDE;
			case CL_DEVICE_NOT_FOUND:
				break;
			default:
			       goto ERR;
		}
	}
OUTSIDE:
	free( platforms );
	platforms = NULL;

	context = clCreateContext( NULL, 1, &device, NULL, NULL, &err );
	if( err < 0 ){ goto ERR; }

	program = clCreateProgramWithSource( context, 1,
			(void*)&vec_source, &source_len, &err );
	if( err < 0 ){ goto ERR; }
	err = clBuildProgram( program, 1, &device, "-cl-std=CL3.0",
			NULL, NULL );
	if( err < 0 ){
		clGetProgramBuildInfo( program, device, CL_PROGRAM_BUILD_LOG,
				0, NULL, &error_len );
		error_log = malloc( error_len );
		clGetProgramBuildInfo( program, device, CL_PROGRAM_BUILD_LOG,
				error_len, error_log, NULL );
		printf( "%s\n", error_log );
		free( error_log );
	       	goto ERR;
	}

	kernel = clCreateKernel( program, "add", &err );
	if( err < 0 ){ goto ERR; }

	queue = clCreateCommandQueueWithProperties( context, device,
			NULL, &err );
	if( err < 0 ){ goto ERR; }

	while( scanf( "%u", &vec_len ) == 1 ){
		vecA = malloc( sizeof( *vecA ) * vec_len );
		vecB = malloc( sizeof( *vecB ) * vec_len );
		vecC = malloc( sizeof( *vecC ) * vec_len );
		
		/* Get vecA, vecB */
		for( i = 0; i < vec_len; i++ ){
			scanf( "%lf", vecA + i );
		}
		for( i = 0; i < vec_len; i++ ){
			scanf( "%lf", vecB + i );
		}

		vecA_cl = clCreateBuffer( context, CL_MEM_READ_ONLY,
				vec_len * sizeof( *vecA ), NULL, &err );
		if( err < 0 ){ goto ERR; }
		vecB_cl = clCreateBuffer( context, CL_MEM_READ_ONLY,
				vec_len * sizeof( *vecB ), NULL, &err );
		if( err < 0 ){ goto ERR; }
		vecC_cl = clCreateBuffer( context, CL_MEM_WRITE_ONLY,
				vec_len * sizeof( *vecC ), NULL, &err );
		if( err < 0 ){ goto ERR; }

		/* Copy vectos to device */
		err = clEnqueueWriteBuffer( queue, vecA_cl, CL_FALSE, 0,
				vec_len * sizeof( *vecA ), vecA, 0, NULL, NULL );
		if( err < 0 ){ goto ERR; }
		err = clEnqueueWriteBuffer( queue, vecB_cl, CL_FALSE, 0,
				vec_len * sizeof( *vecB ), vecB, 0, NULL, NULL );
		if( err < 0 ){ goto ERR; }

		err = clSetKernelArg( kernel, 0, sizeof( cl_mem ), &vecA_cl );
		if( err < 0 ){ goto ERR; }
		err = clSetKernelArg( kernel, 1, sizeof( cl_mem ), &vecB_cl );
		if( err < 0 ){ goto ERR; }
		err = clSetKernelArg( kernel, 2, sizeof( cl_mem ), &vecC_cl );
		if( err < 0 ){ goto ERR; }
		err = clSetKernelArg( kernel, 3, sizeof( vec_len ), &vec_len );
		if( err < 0 ){ goto ERR; }

		/* Get the limit of local work group */
		err = clGetKernelWorkGroupInfo( kernel, device,
				CL_KERNEL_WORK_GROUP_SIZE, sizeof( local_size ),
				&local_size, NULL );
		if( err < 0 ){ goto ERR; }
		global_size = (size_t)ceil( vec_len / (double)local_size ) * local_size;

		/* wait until the queue is empty */
		clFinish( queue );
		err = clEnqueueNDRangeKernel( queue, kernel, 1, NULL,
				&global_size, &local_size, 0, NULL, NULL );
		if( err < 0 ){ goto ERR; }

		clFinish( queue );
		err = clEnqueueReadBuffer( queue, vecC_cl, CL_TRUE, 0,
				vec_len * sizeof( *vecC ), vecC, 0, NULL, NULL);
		if( err < 0 ){ goto ERR; }
		for( i = 0; i < vec_len; i++ ){
			printf( "%lf ", *( vecC + i ) );
		}
		putchar( 10 );

		free( vecA );
		free( vecB );
		free( vecC );
		clReleaseMemObject( vecA_cl );
		clReleaseMemObject( vecB_cl );
		clReleaseMemObject( vecC_cl );

		vecA = vecB = vecC = NULL;
		vecA_cl = vecB_cl = vecC_cl = NULL;
	}
ERR:
	free( platforms );
	free( vecA );
	free( vecB );
	free( vecC );
	/* you created them, so you need to release them */
	clReleaseMemObject( vecA_cl );
	clReleaseMemObject( vecB_cl );
	clReleaseMemObject( vecC_cl );
	clReleaseProgram( program );
	clReleaseContext( context );
	clReleaseKernel( kernel );
	clReleaseCommandQueue( queue );

	return err;
}
