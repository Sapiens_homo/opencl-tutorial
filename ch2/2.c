#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <CL/cl.h>

/* 除錯用 macros */
#define ERR( x ) case x:\
	fprintf( stderr, "%s: %s: %d: %s\n", __FILE__, __func__, __LINE__, #x );\
	break;

#define CLERROR( err ) __extension__({\
	switch( err ){\
		case CL_SUCCESS:\
			break;\
		ERR( CL_DEVICE_NOT_FOUND )\
		ERR( CL_DEVICE_NOT_AVAILABLE )\
		ERR( CL_COMPILER_NOT_AVAILABLE )\
		ERR( CL_MEM_OBJECT_ALLOCATION_FAILURE )\
		ERR( CL_OUT_OF_RESOURCES )\
		ERR( CL_OUT_OF_HOST_MEMORY )\
		ERR( CL_PROFILING_INFO_NOT_AVAILABLE )\
		ERR( CL_MEM_COPY_OVERLAP )\
		ERR( CL_IMAGE_FORMAT_MISMATCH )\
		ERR( CL_BUILD_PROGRAM_FAILURE )\
		ERR( CL_MAP_FAILURE )\
		ERR( CL_MISALIGNED_SUB_BUFFER_OFFSET )\
		ERR( CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST )\
		ERR( CL_COMPILE_PROGRAM_FAILURE )\
		ERR( CL_LINKER_NOT_AVAILABLE )\
		ERR( CL_LINK_PROGRAM_FAILURE )\
		ERR( CL_DEVICE_PARTITION_FAILED )\
		ERR( CL_KERNEL_ARG_INFO_NOT_AVAILABLE )\
		ERR( CL_INVALID_VALUE )\
		ERR( CL_INVALID_DEVICE_TYPE )\
		ERR( CL_INVALID_PLATFORM )\
		ERR( CL_INVALID_DEVICE )\
		ERR( CL_INVALID_CONTEXT )\
		ERR( CL_INVALID_QUEUE_PROPERTIES )\
		ERR( CL_INVALID_COMMAND_QUEUE )\
		ERR( CL_INVALID_HOST_PTR )\
		ERR( CL_INVALID_MEM_OBJECT )\
		ERR( CL_INVALID_IMAGE_FORMAT_DESCRIPTOR )\
		ERR( CL_INVALID_IMAGE_SIZE )\
		ERR( CL_INVALID_SAMPLER )\
		ERR( CL_INVALID_BINARY )\
		ERR( CL_INVALID_BUILD_OPTIONS )\
		ERR( CL_INVALID_PROGRAM )\
		ERR( CL_INVALID_PROGRAM_EXECUTABLE )\
		ERR( CL_INVALID_KERNEL_NAME )\
		ERR( CL_INVALID_KERNEL_DEFINITION )\
		ERR( CL_INVALID_KERNEL )\
		ERR( CL_INVALID_ARG_INDEX )\
		ERR( CL_INVALID_ARG_VALUE )\
		ERR( CL_INVALID_ARG_SIZE )\
		ERR( CL_INVALID_KERNEL_ARGS )\
		ERR( CL_INVALID_WORK_DIMENSION )\
		ERR( CL_INVALID_WORK_GROUP_SIZE )\
		ERR( CL_INVALID_WORK_ITEM_SIZE )\
		ERR( CL_INVALID_GLOBAL_OFFSET )\
		ERR( CL_INVALID_EVENT_WAIT_LIST )\
		ERR( CL_INVALID_EVENT )\
		ERR( CL_INVALID_OPERATION )\
		ERR( CL_INVALID_GL_OBJECT )\
		ERR( CL_INVALID_BUFFER_SIZE )\
		default:\
			fprintf( stderr, "OpenCL Error\n" );\
			break;\
	}\
})

/* 出錯跳掉 OWO */
#define OWO if( err < 0 ){ CLERROR( err ); goto ERR; }

int main( void ){
	cl_int err;
	register cl_uint i;
	register size_t ii;
	register size_t iii;
	register size_t iv;

	/* Source Code, or get from files */
	const char* const mul_src = 
		"__kernel void mul( __global double *A, __global double *B, __global double *C, ulong m, ulong n, ulong p ){	\
			size_t i = get_global_id(0);							\
			size_t j = get_global_id(1);							\
			size_t k = i * m + j;								\
			size_t I;									\
			double s = 0;									\
			if( i < m && j < n ){								\
				for( I = 0; I < p; I++ ){						\
					s += *( A +i*n + I ) * *( B + I*n + j );			\
				}									\
			}										\
			*( C + k ) = s;									\
		}";
	const size_t mul_len = strlen( mul_src );

	/*
	 * Matrix A: mxp
	 * Matrix B: pxn
	 * Matric C = AB = mxn
	 * C_{i, j} = A_{i,1}B_{1,j} +...+ A_{i,p}B_{p,j}
	 */

	register double *A = NULL;
	register double *B = NULL;
	register double *C = NULL;

	size_t m, n , p;

	cl_mem A_cl = NULL;
	cl_mem B_cl = NULL;
	cl_mem C_cl = NULL;

	cl_platform_id *plats = NULL;
	cl_uint n_plats;
	cl_device_id dev = NULL;

	cl_context ctx = NULL;
	cl_program prog = NULL;
	cl_kernel ker = NULL;
	cl_command_queue que = NULL;

	size_t gsize[2];
	size_t lsize;

	register char *compile_err = NULL;
	size_t compile_err_len;

	err = clGetPlatformIDs( 0, NULL, &n_plats );
	OWO;
PLATS_ALLOC:
	plats = malloc( sizeof( plats ) * n_plats );
	if( !plats ){ goto PLATS_ALLOC; }

	err = clGetPlatformIDs( n_plats, plats, NULL );
	OWO;
	for( i = 0; i < n_plats; i++ ){
		err = clGetDeviceIDs( *(plats + i), CL_DEVICE_TYPE_GPU, 1, &dev, NULL );
		switch(err){
			case CL_SUCCESS:
				goto OUTSIDE;
			case CL_DEVICE_NOT_FOUND:
				break;
			default:
				goto ERR;
		}
	}
OUTSIDE:

	ctx = clCreateContext( NULL, 1, &dev, NULL, NULL, &err );
	OWO;
	que = clCreateCommandQueueWithProperties( ctx, dev, NULL, &err );
	OWO;

	/* or clCreateProgramWithBinary */
	prog = clCreateProgramWithSource( ctx, 1, (void*)&mul_src, &mul_len, &err );
	OWO;
	err = clBuildProgram( prog, 0, NULL, NULL, NULL, NULL );
	if( err < 0 ){
		err = clGetProgramBuildInfo( prog, dev, CL_PROGRAM_BUILD_LOG, 0, NULL, &compile_err_len );
		OWO;
MALLOC_COMPILE_ERR:
		compile_err = malloc( compile_err_len );
		if( !compile_err ){ goto MALLOC_COMPILE_ERR; }
		err = clGetProgramBuildInfo( prog, dev, CL_PROGRAM_BUILD_LOG, compile_err_len, compile_err, NULL );
		OWO;
		fprintf( stderr, "%s\n", compile_err );
		free( compile_err );
		compile_err = NULL;
	}
	ker = clCreateKernel( prog, "mul", &err );
	OWO;

	err = clGetKernelWorkGroupInfo( ker, dev, CL_KERNEL_WORK_GROUP_SIZE, sizeof(lsize), &lsize, NULL );
	OWO;

	while( scanf( "%lu%lu%lu", &m, &p, &n ) == 3 ){
MALLOC_A:
		A = malloc( sizeof(*A) * m * p );
		if( !A ){ goto MALLOC_A; }
MBLLOC_B:
		B = malloc( sizeof(*B) * p * n );
		if( !B ){ goto MBLLOC_B; }
MCLLOC_C:
		C = malloc( sizeof(*C) * m * n );
		if( !C ){ goto MCLLOC_C; }

		for( ii = 0; ii < m * p; ii++ ){
			scanf( "%lf", A + ii );
		}

		for( ii = 0; ii < p * n; ii++ ){
			scanf( "%lf", B + ii );
		}

		A_cl = clCreateBuffer( ctx, CL_MEM_READ_ONLY, sizeof(*A) * m * p, NULL, &err );
		OWO;
		B_cl = clCreateBuffer( ctx, CL_MEM_READ_ONLY, sizeof(*B) * p * n, NULL, &err );
		OWO;
		C_cl = clCreateBuffer( ctx, CL_MEM_WRITE_ONLY, sizeof(*C) * m * n, NULL, &err );
		OWO;

		err = clEnqueueWriteBuffer( que, A_cl, CL_TRUE, 0, sizeof(*A) * m * p, A, 0, NULL, NULL );
		OWO;
		err = clEnqueueWriteBuffer( que, B_cl, CL_TRUE, 0, sizeof(*B) * p * n, B, 0, NULL, NULL );
		OWO;

		err = clSetKernelArg( ker, 0, sizeof( cl_mem ), &A_cl );
		OWO;
		err = clSetKernelArg( ker, 1, sizeof( cl_mem ), &B_cl );
		OWO;
		err = clSetKernelArg( ker, 2, sizeof( cl_mem ), &C_cl );
		OWO;
		err = clSetKernelArg( ker, 3, sizeof( m ), &m );
		OWO;
		err = clSetKernelArg( ker, 4, sizeof( n ), &n );
		OWO;
		err = clSetKernelArg( ker, 5, sizeof( p ), &p );
		OWO;

		gsize[0] = (size_t)ceil( (double)m / lsize ) * lsize;
		gsize[1] = (size_t)ceil( (double)n / lsize ) * lsize;
		//err = clEnqueueNDRangeKernel( que, ker, 2, NULL, gsize, lsize, 0, NULL, NULL );
		err = clEnqueueNDRangeKernel( que, ker, 2, NULL, gsize, NULL, 0, NULL, NULL );
		OWO;
		err = clFinish( que );
		OWO;
		err = clEnqueueReadBuffer( que, C_cl, CL_TRUE, 0, sizeof(*C) * m * n, C, 0, NULL, NULL );
		OWO;

		for( iv = ii = 0; ii < m; ii++ ){
			for( iii = 0; iii < n; iii++, iv++ ){
				printf( "%lf ", *(C + iv ) );
			}
			putchar( 10 );
		}

		free( A );
		free( B );
		free( C );
		clReleaseMemObject( A_cl );
		clReleaseMemObject( B_cl );
		clReleaseMemObject( C_cl );
		A = B = C = NULL;
		A_cl = B_cl = C_cl = NULL;
	}

ERR:
	free( A );
	free( B );
	free( C );
	clReleaseMemObject( A_cl );
	clReleaseMemObject( B_cl );
	clReleaseMemObject( C_cl );
	clReleaseCommandQueue( que );
	clReleaseKernel( ker );
	clReleaseProgram( prog );
	clReleaseContext( ctx );
	free( plats );
	free( compile_err );

	return err;
}
